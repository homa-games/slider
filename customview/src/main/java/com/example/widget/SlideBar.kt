package com.example.widget

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.SeekBar
import android.widget.TextView

/**
 * Кнопка с перетягиваемым ползунком
 */
class SlideBar @JvmOverloads constructor(context: Context,
                                         attrs: AttributeSet? = null,
                                         defStyleAttr: Int = 0)
    : FrameLayout(context, attrs, defStyleAttr) {

    private val seekBar: SeekBar
    private val textView: TextView
    private val completeLevel = 85
    private val leftToRight = 0
    private val rightToLeft = 1
    private var listener: (() -> Unit)? = null

    init {
        val view = View.inflate(context, R.layout.slidebar_layout, this)
        val array = context.obtainStyledAttributes(attrs, R.styleable.SlideBar)
        val colorRes = if (Build.VERSION.SDK_INT >= 23) {
            context.getColor(R.color.color_default)
        } else {
            context.resources.getColor(R.color.color_default)
        }
        val color = array.getColor(R.styleable.SlideBar_sb_color, colorRes)
        val orientation = array.getIndex(R.styleable.SlideBar_sb_orientation)
        val text = array.getText(R.styleable.SlideBar_sb_text)
        array.recycle()
        with(view) {
            seekBar = findViewById(R.id.slide)
            textView = findViewById(R.id.text)
        }
        configureSeekBar(color)
        configureTextView(text)
        setOrientation(orientation)
    }

    fun setOnCompleteListener(l: () -> Unit) {
        listener = l
    }

    fun setText(text: String) {
        textView.text = text
    }

    private fun configureSeekBar(color: Int) {
        with(seekBar) {
            (background.mutate() as GradientDrawable).setStroke(-1, color)
            val thumbDrawable = (thumb as LayerDrawable).findDrawableByLayerId(R.id.button_background)
            (thumbDrawable.mutate() as GradientDrawable).setColor(color)
            setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    textView.alpha = 1.0f - progress.toFloat() / 100.0f
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) { }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    if (progress > completeLevel) {
                        listener?.invoke()
                    }
                    seekBar?.progress = 0
                }
            })
        }
    }

    private fun configureTextView(text: CharSequence) {
        with(textView) {
            setText(text)
        }
    }

    private fun setOrientation(orientation: Int) {
        val layoutParams = (textView.layoutParams as LayoutParams)
        val angle = if (orientation == rightToLeft) {
            layoutParams.gravity = Gravity.LEFT
            180.0f
        } else {
            layoutParams.gravity = Gravity.RIGHT
            0.0f
        }
        textView.layoutParams = layoutParams
        seekBar.rotation = angle
    }
}
