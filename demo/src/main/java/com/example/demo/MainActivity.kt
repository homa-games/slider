package com.example.demo

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import com.example.widget.SlideBar

class MainActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<SlideBar>(R.id.sb_first).setOnCompleteListener {
            Toast.makeText(this, "first complete", Toast.LENGTH_SHORT).show()
        }

        findViewById<SlideBar>(R.id.sb_second).setOnCompleteListener {
            Toast.makeText(this, "second complete", Toast.LENGTH_SHORT).show()
        }
    }
}